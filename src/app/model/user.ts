export class User {
    constructor(
        public token: string, 
        public imageURL: string, 
        public name: string, 
        public email: string, 
        public provider: string, 
        public provider_id: string, 
        public token_type: string, 
        public expires_at: string, 
        public expires_in: string, 
        public display_name:string,
        public access_token:string
        ){}
}