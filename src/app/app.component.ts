import { Component, OnInit, NgZone, Input, Output, EventEmitter } from '@angular/core';
// import { AuthService, AppGlobals } from 'angular2-google-login';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { User } from './model/user';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http: Http){}
  
  title = 'app';
  public body;
  public response;
  private Url = 'http://localhost:9000/users';
  onPost(body){
  this.post(this.body).
  subscribe(
    response => this.response = response,
    error => console.log(error)
  );
}
  


post (  body: Object): Observable<User[]> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Access-Control-Allow-Origin': '*' }); // ... Set content type to JSON
        headers.append('Content-Type', 'application/json; charset=UTF-8');
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.Url, bodyString  , options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
}
