import { ApiPage } from './app.po';

describe('api App', () => {
  let page: ApiPage;

  beforeEach(() => {
    page = new ApiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
